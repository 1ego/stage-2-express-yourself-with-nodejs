const fs = require('fs');

const saveData = (data) => {
  
  if (data) {
    fs.writeFile('characterList.json', JSON.stringify(data), (err) => {
      console.log(err);
    });
    return true;
  } else {
    return false;
  }
}

module.exports = {
  saveData
};
