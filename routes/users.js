var express = require('express');
var router = express.Router();

const {
  getName,
  getUserById,
  changeUserById,
  saveName,
  deleteUserById
} = require('../services/user.service');
const {
  isAuthorized
} = require('../middlewares/auth.middleware');
const fs = require('fs');

router.get('/', function (req, res) {
  const result = getName();
  if (result) {
    res.send(result);
  } else {
    res.status(400).send(`Some error`);
  }
});
router.get('/:id', function (req, res) {
  const result = getUserById(req.params.id);
  if (result) {
    res.send(result);
  } else {
    res.status(400).send(`Some error`);
  }
});
router.delete('/:id', function (req, res) {
  const result = deleteUserById(req.params.id);
  if (result) {
    res.send(result);
  } else {
    res.status(400).send(`Some error`);
  }
});
router.put('/:id', function (req, res) {
  const result = changeUserById(req.params.id, req.body);
  if (result) {
    res.send(result);
  } else {
    res.status(400).send(`Some error`);
  }
});

router.post('/', function (req, res) {
  const result = saveName(req.body);
  if (result) {
    res.send(result);
  } else {
    res.status(400).send(`Some error`);
  }
});

module.exports = router;