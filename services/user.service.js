const { saveData } = require('../repositories/user.repository');
const fs = require('fs');

const getName = () => {
  return JSON.parse(fs.readFileSync('characterList.json'));
};
const getUserById = (id) => {
  if (id) {
    const array = getName();
    let object = array.find(item => item._id === id);
    return object;
  } else {
    return null;
  }
};
const changeUserById = (id, user) => {
  if (id, user) {
    let users = getName();
    const index = users.findIndex(user => user._id === id);
    users[index] = user;
    return saveData(users);
  } else {
    return null;
  }
};

const saveName = (user) => {
  if (user) {
    let users = getName();
    users.push(user);
    return saveData(users);
  } else {
    return null;
  }
};
const deleteUserById = (id) => {
  if (id) {
    let users = getName();
    const filteredUsers = users.filter(item => item._id !== id);
    return saveData(filteredUsers);
  } else {
    return null;
  }
};
module.exports = {
  getName,
  getUserById,
  changeUserById,
  saveName,
  deleteUserById
};